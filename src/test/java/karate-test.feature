Feature: Prueba de tienda

  Scenario: Buscar orden por identificacion
  Given url 'https://petstore3.swagger.io/api/v3/store/order/10'
    When method GET
    Then status 200

  Scenario: Añadir orden
    Given url 'https://petstore3.swagger.io/api/v3/store/order'
    When request
    """
    {
      "id": 10,
      "petId": 198772,
      "quantity": 7,
      "shipDate": "2024-01-12T18:30:13.987Z",
      "status": "approved",
      "complete": true
    }
    """
    And method POST
    Then status 200


  Scenario: Eliminar orden
    Given url 'https://petstore3.swagger.io/api/v3/store/order/10'
    When method DELETE
    Then status 200

