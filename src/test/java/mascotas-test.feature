Feature: Prueba Mascotas

  Scenario: Agregar mascota
    Given url 'https://petstore3.swagger.io/api/v3/pet'
    When request
    """
    {
      "id": 100,
      "name": "best pet",
      "category": {
        "id": 1,
          "name": "Dogs"
        },
        "photoUrls": [
          "urlphoto"
        ],
        "tags": [
          {
            "id": 0,
            "name": "string"
          }
        ],
        "status": "available"
    }
    """
    And method POST
    Then status 200


  Scenario: Agregar mascota con identificacion
    Given url 'https://petstore3.swagger.io/api/v3/pet/100?name=newname&status=available'
    When method POST
    Then status 200

  Scenario: Agregar imagen a mascota
    Given url 'https://petstore3.swagger.io/api/v3/pet/100?name=newname&status=available'
    When method POST
    Then status 200


  Scenario: Buscar por estado
    Given url 'https://petstore3.swagger.io/api/v3/pet/findByStatus?status=available' 
    When method GET
    Then status 200

  Scenario: Buscar por categoria
    Given url 'https://petstore3.swagger.io/api/v3/pet/findByTags?tags=dog'
    When method GET
    Then status 200

  Scenario: Buscar por identificacion
    Given url 'https://petstore3.swagger.io/api/v3/pet/10'
    When method GET
    Then status 200
  
  Scenario: Actualizar mascota
    Given url 'https://petstore3.swagger.io/api/v3/pet'
    When request 
    """
    {
      "id": 10,
      "name": "doggie",
      "category": {
        "id": 1,
        "name": "Dogs"
      },
      "photoUrls": [
        "string"
      ],
      "tags": [
        {
          "id": 0,
          "name": "string"
        }
      ],
      "status": "available"
    }
    """
    And method PUT
    Then status 200