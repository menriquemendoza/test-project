Feature: Pruebas Usuarios

    Scenario: Agregar usuario
    Given url 'https://petstore3.swagger.io/api/v3/user'
        When request 
        """
        {
            "id": 10,
            "username": "theUser",
            "firstName": "John",
            "lastName": "James",
            "email": "john@email.com",
            "password": "12345",
            "phone": "12345",
            "userStatus": 1 
        }
        """
        And method POST
        Then status 200

    Scenario: Agregar lista de usuarios
        Given url 'https://petstore3.swagger.io/api/v3/user/createWithList'
        When request
        """
        [
            {
                "id": 10,
                "username": "theUser",
                "firstName": "John",
                "lastName": "James",
                "email": "john@email.com",
                "password": "12345",
                "phone": "12345",
                "userStatus": 1
            }
        ]
        """
        And method POST 
        The status 200

    Scenario: Filtrar por nombre de usuario y contraseña
        Given url 'https://petstore3.swagger.io/api/v3/user/login?username=theUser&password=thePass'
        When method GET
        Then status 200

    Scenario: Obtener logout
        Given url 'https://petstore3.swagger.io/api/v3/user/logout'
        When method GET
        Then status 200
    
    Scenario: Buscar por nomrbe de usuario
        Given url 'https://petstore3.swagger.io/api/v3/user/theUser'
        When method GET
        Then status 200

    Scenario: Actualizar por nombre de usuario 
        Given url 'https://petstore3.swagger.io/api/v3/user/theUser'
        When request 
        """
            {
                "id": 10,
                "username": "elUsuario",
                "firstName": "John",
                "lastName": "James",
                "email": "john@email.com",
                "password": "12345",
                "phone": "12345",
                "userStatus": 1
            }
        """
        And method PUT
        Then status 200

    Scenario: Eliminar por nombre de usuario
        Given url 'https://petstore3.swagger.io/api/v3/user/theUser'
        When method DELETE
        Then status 200